//Create an OscRecv object:
OscRecv orec;
//Tell the OscRecv object the port:
13000 => orec.port;
//Tell the OscRecv object to start listening for OSC messages on that port:
orec.listen();

function void rate_control_shred()
{
    orec.event("/conductor/downbeat,f") @=> OscEvent rate_event; 
    //orec.event("eeg") @=> OscEvent oscdata;
    while(true)
    {
        rate_event => now; //wait for events to arrive.
        PercFlut i => JCRev r => WvIn e => dac;
        r => dac;
        .6 => e.gain;
        .05 => r.mix;
        0.5 => i.gain;
        //while(true)
        //{
            Math.random2f(30,1000) => i.freq;
            1 => i.noteOn;
            100::ms => now;
            0 => i.noteOff;
        //}
    }
}
