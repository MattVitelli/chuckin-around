//Create an OscSend object:
OscSend xmit;
//Set the host and port of this object:
xmit.setHost("localhost", 13000);
//To send a message with no contents:
xmit.startMsg("/conductor/downbeat");
