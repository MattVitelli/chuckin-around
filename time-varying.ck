// FM time-varying - Madeline Huberth - 1/18/14
//like the frequency at which this pulses
SinOsc m => SqrOsc c => ResonZ f => JCRev r => dac;
// gain
.5 => r.gain;
// reverb mix
.05 => r.mix;

// sync for FM synthesis
2 => c.sync;

// carrier freq
1000 =>  c.freq;
// modulator freq
330 => m.freq;
// index of modulation
90 => m.gain;

// go 
while( true )
{
    c.freq()*1.5 + c.freq()*
    Math.sin( now/second*.9 ) => f.freq;
    8 => f.Q;    
    // time step
    5::ms => now;
}
