// create our OSC receiver
OscIn oin;
// create our OSC message
OscMsg msg;
// use port 6449
6449 => oin.port;
// create an address in the receiver
oin.addAddress( "/sndbuf/buf/rate, f" );
//PulseOsc i => JCRev r => WvIn e => dac;
SinOsc m => SqrOsc c => ResonZ f => JCRev r => dac;
r => dac;
.6 => e.gain;
.05 => r.mix;
0.5 => i.gain;

// infinite event loop
while ( true )
{
    // wait for event to arrive
  //  oin => now;
    
    Math.random2f(30,1000) => i.freq;
    1 => i.noteOn;
    100::ms => now;
    0 => i.noteOff;

    // grab the next message from the queue. 
  /*  while ( oin.recv(msg) != 0 )
    { 
        //while(true)
        //{
        //    msg.getFloat(0) * 30000.0 => i.freq;
            1 => i.noteOn;
            100::ms => now;
            0 => i.noteOff;
        //}
        // getFloat fetches the expected float (as indicated by "f")
     //   msg.getFloat(0) => buf.play;
        // print
        <<< "got (via OSC):", i.freq >>>;
        // set play pointer to beginning
     //   0 => buf.pos;
    }
    */
}
