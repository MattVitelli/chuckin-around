﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ventuz.OSC;
using System.Net;
using System.Net.Sockets;

namespace MattNeuroSkyApp
{
    class ChuckWriter
    {
        string hostname = "127.0.0.1"; //localhost
        string chuckname = "/sndbuf/buf/rate"; //URL for ChucK
        int port = 6449; //Port for ChucK
        UdpWriter writer;
        public ChuckWriter()
        {
            writer = new UdpWriter(hostname, port);
        }

        public void Write(float value)
        {
            OscElement elem = new OscElement(chuckname, value);
            writer.Send(elem);
        }
    }
}
