﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MattNeuroSkyApp
{
    public class GFXPrimitives
    {
        public ScreenAlignedQuad Quad;

        public GFXPrimitives()
        {
            Quad = new ScreenAlignedQuad();
        }
    }

    public class ScreenAlignedQuad
    {
        #region Private Members
        VertexPositionTexture[] verts = null;
        short[] ib = null;
        #endregion


        #region Constructor
        public ScreenAlignedQuad()
        {
            verts = new VertexPositionTexture[]
                        {
                            new VertexPositionTexture(
                                new Vector3(0,0,0),
                                new Vector2(1,1)),
                            new VertexPositionTexture(
                                new Vector3(0,0,0),
                                new Vector2(0,1)),
                            new VertexPositionTexture(
                                new Vector3(0,0,0),
                                new Vector2(0,0)),
                            new VertexPositionTexture(
                                new Vector3(0,0,0),
                                new Vector2(1,0))
                        };

            ib = new short[] { 0, 1, 2, 2, 3, 0 };
        }
        #endregion

        public void SetTexCoords(Vector2 _min, Vector2 _max)
        {
            verts[0].TextureCoordinate = _max;
            verts[1].TextureCoordinate = new Vector2(_min.X, _max.Y);
            verts[2].TextureCoordinate = _min;
            verts[3].TextureCoordinate = new Vector2(_max.X, _min.Y);
        }

        #region void Render(Vector2 v1, Vector2 v2)
        public void Render(Vector2 v1, Vector2 v2)
        {
            IGraphicsDeviceService graphicsService = (IGraphicsDeviceService)
                base.Game.Services.GetService(typeof(IGraphicsDeviceService));

            GraphicsDevice device = graphicsService.GraphicsDevice;
            device.VertexDeclaration = vertexDecl;

            verts[0].Position.X = v2.X;
            verts[0].Position.Y = v1.Y;

            verts[1].Position.X = v1.X;
            verts[1].Position.Y = v1.Y;

            verts[2].Position.X = v1.X;
            verts[2].Position.Y = v2.Y;

            verts[3].Position.X = v2.X;
            verts[3].Position.Y = v2.Y;

            device.DrawUserIndexedPrimitives<VertexPositionTexture>
                (PrimitiveType.TriangleList, verts, 0, 4, ib, 0, 2);
        }

        public void Render()
        {
            IGraphicsDeviceService graphicsService = (IGraphicsDeviceService)
                base.Game.Services.GetService(typeof(IGraphicsDeviceService));

            GraphicsDevice device = graphicsService.GraphicsDevice;
            device.VertexDeclaration = vertexDecl;

            verts[0].Position.X = 1;
            verts[0].Position.Y = -1;

            verts[1].Position.X = -1;
            verts[1].Position.Y = -1;

            verts[2].Position.X = -1;
            verts[2].Position.Y = 1;

            verts[3].Position.X = 1;
            verts[3].Position.Y = 1;

            device.DrawUserIndexedPrimitives<VertexPositionTexture>
                (PrimitiveType.TriangleList, verts, 0, 4, ib, 0, 2);
        }
        #endregion
    }
}
