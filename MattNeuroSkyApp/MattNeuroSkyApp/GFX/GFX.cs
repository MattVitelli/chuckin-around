﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MattNeuroSkyApp
{
    public static class GFX
    {
        public static GFXPrimitives Primitives;
        public static GraphicsDevice Device;

        public GFX(GraphicsDevice _device)
        {
            Primitives = new GFXPrimitives();
            Device = _device;
        }
    }
}
