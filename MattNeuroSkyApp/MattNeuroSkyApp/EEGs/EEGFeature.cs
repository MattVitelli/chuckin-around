﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MattNeuroSkyApp.EEGs
{
    public struct EEGFeature
    {
        public EEGFeature(float alpha1, float alpha2, float beta1, float beta2,
            float delta, float gamma1, float gamma2, float theta, float raw)
        {
            this.alpha1 = alpha1;
            this.alpha2 = alpha2;
            this.beta1 = beta1;
            this.beta2 = beta2;
            this.delta = delta;
            this.gamma1 = gamma1;
            this.gamma2 = gamma2;
            this.theta = theta;
            this.raw = raw;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0} ", alpha1);
            builder.AppendFormat("{0} ", alpha2);
            builder.AppendFormat("{0} ", beta1);
            builder.AppendFormat("{0} ", beta2);
            builder.AppendFormat("{0} ", delta);
            builder.AppendFormat("{0} ", gamma1);
            builder.AppendFormat("{0} ", gamma2);
            builder.AppendFormat("{0} ", theta);
            builder.AppendFormat("{0}", raw);
            return builder.ToString();
        }

        public string PrintFeatures()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("Alpha1 {0}\n", alpha1);
            builder.AppendFormat("Alpha2 {0}\n", alpha2);
            builder.AppendFormat("Beta1 {0}\n", beta1);
            builder.AppendFormat("Beta2 {0}\n", beta2);
            builder.AppendFormat("Delta {0}\n", delta);
            builder.AppendFormat("Gamma1 {0}\n", gamma1);
            builder.AppendFormat("Gamma2 {0}\n", gamma2);
            builder.AppendFormat("Theta {0}\n", theta);
            builder.AppendFormat("Raw {0}\n", raw);
            return builder.ToString();
        }

        float alpha1;
        float alpha2;
        float beta1;
        float beta2;
        float delta;
        float gamma1;
        float gamma2;
        float theta;
        float raw;
    }
}
