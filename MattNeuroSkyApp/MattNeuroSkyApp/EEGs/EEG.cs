﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.IO.Ports;

namespace MattNeuroSkyApp.EEGs
{
    public class EEG
    {
        string comPort = "COM12";
        int connectID = -1;
        Timer timer = null;
        ElapsedEventHandler callback = null;
        EEGFeature feature;
        ChuckWriter chuckInterface;

        public EEG(ElapsedEventHandler callback)
        {
            this.callback = callback;
        }

        public bool Connected
        {
            get { return connectID >= 0; }
        }

        public EEGFeature GetFeature()
        {
            return feature;
        }

        float minValue = float.PositiveInfinity;
        float maxValue = float.NegativeInfinity;

        void Update(object sender, EventArgs e)
        {

            int packetCount = ThinkGear.TG_ReadPackets(connectID, -1);
            feature = new EEGFeature(ThinkGear.TG_GetValue(connectID, ThinkGear.DATA_ALPHA1),
                ThinkGear.TG_GetValue(connectID, ThinkGear.DATA_ALPHA2), ThinkGear.TG_GetValue(connectID, ThinkGear.DATA_BETA1),
                ThinkGear.TG_GetValue(connectID, ThinkGear.DATA_BETA2), ThinkGear.TG_GetValue(connectID, ThinkGear.DATA_DELTA),
                ThinkGear.TG_GetValue(connectID, ThinkGear.DATA_GAMMA1), ThinkGear.TG_GetValue(connectID, ThinkGear.DATA_GAMMA2),
                ThinkGear.TG_GetValue(connectID, ThinkGear.DATA_THETA), ThinkGear.TG_GetValue(connectID, ThinkGear.DATA_RAW));
            
    //        Console.WriteLine(feature.ToString());

            float value = ThinkGear.TG_GetValue(connectID, ThinkGear.DATA_RAW);
            minValue = Math.Min(value, minValue);
            maxValue = Math.Max(value, maxValue);
            float normalized = value = (value - minValue) / (maxValue - minValue);
            Console.WriteLine("{0}\t{1}\t{2}\t{3}", minValue,maxValue,value,normalized);

            //float value = ThinkGear.TG_GetValue(connectID, ThinkGear.DATA_ALPHA1)/50000.0f;
            chuckInterface.Write(normalized);
        }

        public bool Connect()
        {

            SerialPort SerPort = new SerialPort("COM12");

            try
            {
                SerPort.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error opening port: {0}", ex.Message);
            } 


            connectID = ThinkGear.TG_GetNewConnectionId();
            if (connectID >= 0)
            {
                chuckInterface = new ChuckWriter();
                Console.Write("Received Connection ID!\nDriver version is " + ThinkGear.TG_GetDriverVersion() + "\n");
                int retValue = ThinkGear.TG_Connect(connectID, comPort, ThinkGear.BAUD_9600, ThinkGear.STREAM_PACKETS);

                Console.WriteLine("{0}\t{1}\t{2}\t{3}", retValue, connectID, comPort, ThinkGear.BAUD_9600);

                const double baudRate = 9600.0;
                timer = new Timer();
                timer.Interval = (1000.0 / baudRate); //Conversion of baud rate to milliseconds
                timer.Elapsed += new ElapsedEventHandler(Update);
                timer.Elapsed += callback;
                timer.Start();
            }
            return (connectID >= 0);
        }

        public void Disconnect()
        {
            if (connectID >= 0)
            {
                timer.Stop();
                ThinkGear.TG_Disconnect(connectID);
                ThinkGear.TG_FreeConnection(connectID);
                connectID = -1;
            }
        }
    }
}
