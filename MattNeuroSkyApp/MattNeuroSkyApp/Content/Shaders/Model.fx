uniform float4x4 ModelView;

struct VertexShaderInput
{
    float4 Position : POSITION0;
    float3 Normal	: NORMAL;
    float3 Tangent	: TANGENT;
    float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
    float3 Normal	: TEXCOORD1;
};

VertexShaderOutput BasicVS(VertexShaderInput input)
{
    VertexShaderOutput output;

    output.Position = mul(float4(input.Position.xyz,1), ModelView);
    output.TexCoord = input.TexCoord;
    output.Normal = input.Normal;
    return output;
}

float4 BasicPS(VertexShaderOutput input) : COLOR0
{
	return float4(1,0,0,1);
}

technique Technique1
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 BasicVS();
        PixelShader = compile ps_2_0 BasicPS();
    }
}