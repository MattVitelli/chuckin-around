float3 Color;
float2 Res;
float RepeatCount;
float Gamma;

texture baseMap;
sampler BasePSAMP = sampler_state
{
    Texture = (baseMap);
    
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler BasePSAMPWrap = sampler_state
{
    Texture = (baseMap);
    
    AddressU = WRAP;
    AddressV = WRAP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

struct VSIN
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VSOUT
{
    float4 Position : POSITION;
    float2 TC :	  TEXCOORD0;
};

VSOUT BasicVS(VSIN input)
{
    VSOUT output;

    output.Position = input.Position;
    output.TC = input.TexCoord;
    return output;
}

float4 DrawLinesPS(VSOUT input) : COLOR0
{
	float2 TC = input.TC;
	float2 invRes = 1.0f/Res;
	TC *= RepeatCount;
	return pow(tex2D(BasePSAMPWrap, TC),Gamma);
}

float4 BasicPS(VSOUT input) : COLOR0
{
	return float4(Color,1);
}

technique DrawLines
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 BasicVS();
        PixelShader = compile ps_2_0 DrawLinesPS();
    }
}

technique DisplayColor
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 BasicVS();
        PixelShader = compile ps_2_0 BasicPS();
    }
}