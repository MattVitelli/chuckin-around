float2 Res;
float PlaneCount;
float4x4 ModelView;

texture baseMap;
sampler BasePSAMP = sampler_state
{
    Texture = (baseMap);
    
    AddressU = WRAP;
    AddressV = WRAP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

struct VSIN
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
    float3 Color	: COLOR0;
};

struct VSOUT
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VSOUT_Model
{
    float4 Position : POSITION0;
    float3 Diffuse	: TEXCOORD1;
    float2 TexCoord	: TEXCOORD0;
};

VSOUT VS(VSIN input)
{
    VSOUT output;
    output.Position = input.Position;
    output.TexCoord = input.TexCoord;
    return output;
}

VSOUT_Model VS_Model(VSIN input)
{
    VSOUT_Model output;
    output.Position = mul(float4(input.Position.xyz,1.0), ModelView);
    output.Diffuse = input.Color;
    output.TexCoord = input.TexCoord;
    return output;
}

float4 DrawModelPS(VSOUT_Model input) : COLOR0
{
    return float4(input.Diffuse,1.0);//tex2D(BasePSAMP, float2(input.TexCoord.x, 1-input.TexCoord.y));
}


float4 DrawFringePS(VSOUT input) : COLOR0
{
	float2 invRes = 1.0/Res;
    float2 TC = input.TexCoord+0.5*invRes;
    float4 fringeData = tex2D(BasePSAMP, TC);
    return (fringeData/500)*0.5+0.5;
}

technique DrawModel
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 VS_Model();
        PixelShader = compile ps_2_0 DrawModelPS();
    }
}


technique DrawFringe
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_2_0 DrawFringePS();
    }
}
