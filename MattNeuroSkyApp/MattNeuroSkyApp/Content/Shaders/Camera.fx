#define Inv3 0.333333334
#define Gamma 2.2

float2 Res;
float2 princPoint;
float2 focalPoint;
float2 RadCoeff;
float2 TanCoeff;

float3 Ro;
float4x4 CamMatrix;

float SmoothRadius;

texture baseMap;
sampler BasePSAMP = sampler_state
{
    Texture = (baseMap);
    
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler BaseLINSAMP = sampler_state
{
    Texture = (baseMap);
    
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = LINEAR;
    MinFilter = LINEAR;
    Mipfilter = NONE;
    
};

texture baseMap2;
sampler Base2PSAMP = sampler_state
{
    Texture = (baseMap2);
    
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

uniform extern texture texture_c0;
uniform extern texture texture_c1;
uniform extern texture texture_c2;
uniform extern texture texture_c3;

// Texture samplers.
sampler2D colorSampler[4] : register(S0) = 
{
	sampler_state
	{
		texture		= <texture_c0>;
		AddressU = WRAP; AddressV = WRAP; MagFilter = POINT; MinFilter = POINT; Mipfilter = POINT;
	},
	sampler_state
	{
		texture		= <texture_c1>;
		AddressU = WRAP; AddressV = WRAP; MagFilter = POINT; MinFilter = POINT; Mipfilter = POINT;
	},
	sampler_state
	{
		texture		= <texture_c2>;
		AddressU = WRAP; AddressV = WRAP; MagFilter = POINT; MinFilter = POINT; Mipfilter = POINT;
	},
	sampler_state
	{
		texture		= <texture_c3>;
		AddressU = WRAP; AddressV = WRAP; MagFilter = POINT; MinFilter = POINT; Mipfilter = POINT;
	},
};

struct VSIN
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VSOUT
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

VSOUT VS(VSIN input)
{
    VSOUT output;
    output.Position = input.Position;
    output.TexCoord = input.TexCoord;
    return output;
}

float2 UndistCoord(float2 TC, float2 cPoint, float2 fLen)
{
   float x = TC.x-cPoint.x;
   float y = TC.y-cPoint.y;
   x /= fLen.x;
   y /= fLen.y;
   float x2 = x*x, y2 = y*y;
   float r2 = x2 + y2;
   float _2xy = 2*x*y;
   float kr = 1 + (RadCoeff.y*r2 + RadCoeff.x)*r2;
   float u = (x*kr + TanCoeff.x*_2xy + TanCoeff.y*(r2 + 2*x2));
   float v = (y*kr + TanCoeff.x*(r2 + 2*y2) + TanCoeff.y*_2xy);
  
   return float2(u,v)*fLen+cPoint;
}

float4 UndistortPS(VSOUT input) : COLOR0
{
	float2 invRes = 1.0/Res;
    float2 TC = input.TexCoord;
    float2 pP = princPoint*invRes;
    float2 fP = focalPoint*invRes;
    
    float2 uCrd = UndistCoord(TC, pP, fP)+0.5*invRes;
    return tex2D(BaseLINSAMP, uCrd);
}

float4 DrawImagePS(VSOUT input) : COLOR0
{
	float2 invRes = 1.0/Res;
    float2 TC = input.TexCoord+0.5*invRes;
   
    return tex2D(BasePSAMP, TC);
}

const float gaussianBlur5x5[5][5] =
{
   0.000788907,   0.006581144,   0.013347322,   0.006581144,   0.000788907,
   0.006581144,   0.054900876,   0.111345294,   0.054900876,   0.006581144,
   0.013347322,   0.111345294,   0.225821052,   0.111345294,   0.013347322,
   0.006581144,   0.054900876,   0.111345294,   0.054900876,   0.006581144,
   0.000788907,   0.006581144,   0.013347322,   0.006581144,   0.000788907,
};

struct LightBuffer
{
	float4 Diffuse : COLOR0;
	float4 Specular: COLOR1;
};

LightBuffer RecoverLightField(VSOUT IN) : COLOR
{
	LightBuffer OUT;
	
	float2 invRes = 1.0/Res;
    float2 TC = IN.TexCoord+0.5*invRes;
    float3 diffSrc = pow(tex2D(BasePSAMP, TC),Gamma);
    float3 specSrc = pow(tex2D(Base2PSAMP, TC),Gamma);
    specSrc = max(0.0, specSrc-diffSrc);
	diffSrc *= 2;
	
	OUT.Diffuse = float4(diffSrc, dot(Inv3, diffSrc));
	OUT.Specular = float4(specSrc, dot(Inv3, specSrc));
}

LightBuffer ConvolveImagePS(VSOUT IN) : COLOR
{
	LightBuffer OUT;
	
	float2 invRes = 1.0/Res;
    float2 TC = IN.TexCoord+0.5*invRes;
    
    float4 diffLin = 0;
	float4 specLin = 0;
	for(int x = -2; x < 2; x++)
		for(int y = -2; y < 2; y++)
		{
			float2 Off = float2(x,y)*SmoothRadius*invRes;
			diffLin += tex2D(BasePSAMP, TC+Off)*gaussianBlur5x5[x+2][y+2];
			specLin += tex2D(Base2PSAMP, TC+Off)*gaussianBlur5x5[x+2][y+2];
		}
   
    OUT.Diffuse = diffLin;
    OUT.Specular = specLin;
    return OUT;
}

float4 ComputeNormalPS(VSOUT IN) : COLOR0
{
	float2 invRes = 1.0/Res;
    float2 TC = IN.TexCoord+0.5*invRes;
    float3 Normal = tex2D(BasePSAMP, TC);
    
    float Diff = dot(Inv3,tex2D(BasePSAMP, TC).xyz);
    
    return float4(Normal,1);
}

technique DrawImage
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_2_0 DrawImagePS();
    }
}

technique Undistort
{
    pass Pass1
    {

        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_2_0 UndistortPS();
    }
}