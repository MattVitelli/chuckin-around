﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Timers;

using MattNeuroSkyApp.EEGs;
namespace MattNeuroSkyApp
{
    public partial class MainForm : Form
    {
        const string notConnectedText = "Not Connected";
        const string connectedText = "Connected";
        const string START_TRAIN = "Begin Training";
        const string END_TRAIN = "Stop Training";
        const string THOUGHTS_DIR = "Thoughts";
        const string THOUGHTS_FILENAME = THOUGHTS_DIR+"/Thoughts.txt";
        bool isTraining = false;
        EEG eegDevice;
        SortedList<string, List<List<EEGFeature>>> features;
        List<EEGFeature> currentFeature = null;

        public MainForm()
        {
            InitializeComponent();
            eegDevice = new EEG(new ElapsedEventHandler(OnReceiveEEGData));
            features = new SortedList<string, List<List<EEGFeature>>>();
            LoadThoughts();
        }

        void OnReceiveEEGData(object sender, EventArgs e)
        {
            Console.WriteLine("DataReceived\n");
            if (isTraining)
                currentFeature.Add(eegDevice.GetFeature());
        }

        void SaveFeatureToFile(string featureName, List<EEGFeature> feature)
        {
            string featureDir = THOUGHTS_DIR + "/" + featureName;
            if (!Directory.Exists(featureDir))
                Directory.CreateDirectory(featureDir);
            string filename = DateTime.Now.ToString();
            filename = filename.Replace(':', '_');
            filename = filename.Replace('/', '_');
            string featureFileName = featureDir + "/" + filename + ".txt.";
            using (FileStream fs = new FileStream(featureFileName, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    for (int i = 0; i < feature.Count; i++)
                        writer.WriteLine(feature[i].ToString());
                }
            }

        }

        void LoadThoughts()
        {
            if (!File.Exists(THOUGHTS_FILENAME))
                return;
            using (FileStream fs = new FileStream(THOUGHTS_FILENAME, FileMode.Open))
            {
                using (StreamReader reader = new StreamReader(fs))
                {
                    int numFeatures = int.Parse(reader.ReadLine());
                    for (int i = 0; i < numFeatures; i++)
                        AddThought(reader.ReadLine());
                }
            }
        }

        void SaveThoughts()
        {
            if (!Directory.Exists(THOUGHTS_DIR))
                Directory.CreateDirectory(THOUGHTS_DIR);
            using (FileStream fs = new FileStream(THOUGHTS_FILENAME, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    writer.WriteLine(features.Count);
                    for (int i = 0; i < features.Count; i++)
                    {
                        writer.WriteLine(features.Keys[i]);
                    }
                }
            }
        }

        void AddThought(string name)
        {
            if (!features.ContainsKey(name))
            {
                features.Add(name, new List<List<EEGFeature>>());
                comboBoxName.Items.Add(name);
                comboBoxName.SelectedIndex = comboBoxName.Items.Count-1;
            }
        }

        void RemoveThought(string name)
        {
            if (features.ContainsKey(name))
            {
                features.Remove(name);
                int index = comboBoxName.Items.IndexOf(name);
                comboBoxName.Items.RemoveAt(index);
                comboBoxName.SelectedIndex = 0;
            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (!eegDevice.Connected)
            {
                eegDevice.Connect();
                connectButton.Text = "Disconnect";
            }
            else
            {
                eegDevice.Disconnect();
                connectButton.Text = "Connect";
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveThoughts();
            eegDevice.Disconnect();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            AddThought(textBoxName.Text);
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            RemoveThought((string)comboBoxName.SelectedItem);
        }

        private void buttonTrain_Click(object sender, EventArgs e)
        {
            if (!isTraining)
            {
                buttonTrain.Text = END_TRAIN;
                currentFeature = new List<EEGFeature>();
                comboBoxName.Enabled = false;
            }
            else
            {
                buttonTrain.Text = START_TRAIN;
                string currFeatureName = (string)comboBoxName.Items[comboBoxName.SelectedIndex];
                features[currFeatureName].Add(currentFeature);
                SaveFeatureToFile(currFeatureName, currentFeature);
                comboBoxName.Enabled = true;
            }
            isTraining = !isTraining;
        }
    }
}
